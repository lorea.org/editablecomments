<?php

$english = array(

	/**
	 * Menu items and titles
	 */
	'comment:error'  =>  "Error saving comment",
	'comment:edited' => "Comment Edited",
	'comment:edit' =>"Edit comment",
);

add_translation("en", $english);
