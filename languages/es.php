<?php

$spanish = array( 
	'comment:edited'  =>  "Comentario editado",
	'comment:error'  =>  "Error grabando el comentario",
	'comment:edit' =>"Edita el comentario",
); 

add_translation('es', $spanish); 
