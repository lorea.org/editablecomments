<?php

elgg_register_event_handler('init','system','editablecomments_init');

function editablecomments_init() {
	// extend CSS and Javascript
	elgg_extend_view('css/elgg', 'editablecomments/css');
	elgg_extend_view('js/elgg', 'editablecomments/js');

	// extend the comment view with the form
	elgg_extend_view('annotation/generic_comment', 'editablecomments/generic_comment');
	
	elgg_register_plugin_hook_handler('register', 'menu:annotation', 'editablecomments_annotation_menu');

	// register the update action
	$actions_path = elgg_get_plugins_path() . "editablecomments/actions/editablecomments";
	elgg_register_action('editablecomments/edit', "$actions_path/edit.php");
}

/**
 * Add a menu item to the annotations
 */
function editablecomments_annotation_menu($hook, $type, $return, $params) {
	$url = "#edit-annotation-" . $params['annotation']->id;
	$item = new ElggMenuItem('comment:edit', elgg_echo('edit'), $url);
	$return[] = $item;
	return $return;
}
